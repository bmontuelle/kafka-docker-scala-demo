

import scala.concurrent.Await
import scala.concurrent.duration._



val akkaDriver =  new AkkaDriver

Await.result(akkaDriver.system.whenTerminated, 10 seconds)
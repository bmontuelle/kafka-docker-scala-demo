import fr.hoshi.kafkasandbox.PlainDriver
import org.apache.kafka.clients.consumer.{ConsumerRecord, ConsumerRecords}

import collection.JavaConverters._


val consumer = PlainDriver.consumer

consumer.subscribe(Set("test").asJava)
/*
//polling the topic for n seconds, not very scala-esque
println("Starting to wait for messsages on the test topic for n seconds")
consumer.poll(10000).asScala.map(consumerRecord => {
  println(s"ConsumerRecord $consumerRecord")
})

println("done")*/

//while (true) {
for( a <- 1 to 10) {
  println(a.toString)
  val records: ConsumerRecords[String, String] = consumer.poll(10)
  records.asScala map { record =>
    printf("offset = %d, key = %s, value = %s", record.offset(), record.key(), record.value());
  }
}

consumer.close()

import cakesolutions.kafka.KafkaProducerRecord
import fr.hoshi.kafkasandbox.PlainDriver
import org.apache.kafka.clients.producer.RecordMetadata

import scala.util.{Failure, Success, Try}

val producer = PlainDriver.producer

//Sending string message
producer.send(KafkaProducerRecord("test", "Ok, let's start this demo"))

//Sending with execution control
for( a <- 1 to 10)
  producer.sendWithCallback(KafkaProducerRecord("test", Some(s"key$a"), s"value$a"))((tryRecord: Try[RecordMetadata]) =>
    tryRecord match {
      case Success(recordMetadata) => println (s"Sucess !!! $recordMetadata ")
      case Failure(e) => println(s"Error /!\\ $e")
    }
  )


producer.close()
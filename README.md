# README #

Sample project, used to demonstrate using kafka with the [scala-kafka-client wrapper](https://github.com/cakesolutions/scala-kafka-client)

Not any of the code here is meant for production use, its a sandbox project

## Objectives ##
1. Build a docker-compose project to start zookeeper and kafka dev environment easily [https://hub.docker.com/r/ches/kafka/]
2. Build a simple scala project for an hands-on with Kafka messaging using the scala-kafka-client library
3. Use akka-stream and akka-http to retrieve twitter feed and forward the tweets to kafka
4. Implement REST services with akka-http and and make them communicate asynchronously through kafka (WIP)
  
## Prerequisites ##

* A working docker environment and docker-compose 
* Kafka helpers scripts, available in 
[kafka installation tarball](https://www.apache.org/dyn/closer.cgi?path=/kafka/0.10.1.0/kafka_2.11-0.10.1.0.tgz)
* A working scala 2.11 dev environment, with SBT

## Quick Start ##

### Kafka broker start ###

Start containers and create a topic named 'test'
```bash
$ start_environment.sh
```

Start a consumer on this topic
```bash
$ kafka-console-consumer.sh --topic test --from-beginning --zookeeper localhost:2181
```

On a different console, start a producer for the 'test' topic
```bash
$ kafka-console-producer.sh --topic test --broker-li localhost:9092
> `Type something`
```

The producer appends text to the kafka log topic, and the consumer displays it on its terminal console.
You can start multiple consumers and producers and try broadcasting messages between them.

Also, as Kafka is using filesystem storage for data persistence, you can see its writing in the file
```bash
$ tail -f data/test-0/00000000000000000000.log
```
Once this works without errors we'll see how to communicate through the Kafka 


### Scala scala-kafka-client API ###

#### Using plain driver ####
Plain driver, as refered to by the scala-kafka-client documentation is a thin wrapper around the java kafka driver.
It allows to produce and consume messages to and from kafka

**Producer sample**
```scala
import cakesolutions.kafka.{KafkaProducer, KafkaProducerRecord}
import org.apache.kafka.common.serialization.StringSerializer

val producer = KafkaProducer(
    KafkaProducer.Conf(
      new StringSerializer(),
      new StringSerializer(),
      bootstrapServers = "localhost:9092"
    )
)
producer.send(KafkaProducerRecord("test", "Ok, let's start this demo"))
```

**Consumer example**
```scala
import cakesolutions.kafka.KafkaConsumer
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.clients.consumer.{ConsumerRecord, ConsumerRecords}

import collection.JavaConverters._

val consumer = KafkaConsumer(KafkaConsumer.Conf(
                             new StringDeserializer(),
                             new StringDeserializer(),
                             bootstrapServers = "localhost:9092",
                             groupId = "group"))

//subscribe to one or multiple topics
consumer.subscribe(Set("test").asJava)

//Now you can poll akka to get new messages on the topic
while(true) {
  //consumer.poll will block the thread for 100ms and if messages are added in the topic they'll be available in "records"
  val records: ConsumerRecords[String, String] = consumer.poll(100)
  records.asScala map { record =>
    printf("offset = %d, key = %s, value = %s", record.offset(), record.key(), record.value());
  }
}

consumer.close()

```
Notice we had to use a main loop to continue polling on kafka, and collections.JavaConverters to pass from topics set to
 Java Iterable and from java iterable Records to scala iterable
 

#### Using Akka Actors ####
Configuration
```properties
// Standard KafkaConsumer properties:
bootstrap.servers = "localhost:9092",
group.id = "group"
enable.auto.commit = false
auto.offset.reset = "earliest"

// KafkaConsumerActor config
// scheduling interval for Kafka polling when consumer is inactive
schedule.interval = 1 second
// duration for how long to wait for a confirmation before redelivery
unconfirmed.timeout = 3 seconds
// maximum number of times a unconfirmed message will be redelivered
max.redeliveries= 3
```

**Producer minimal implementation**
```scala
import akka.actor.ActorSystem
import cakesolutions.kafka.akka.{KafkaProducerActor, ProducerRecords}
import com.typesafe.config.ConfigFactory
import org.apache.kafka.common.serialization.StringSerializer

val system = ActorSystem("akkaSandboxSystem")
val conf = ConfigFactory.load()

val producer = system.actorOf(
  KafkaProducerActor.props(conf, new StringSerializer, new StringSerializer)
)

// Send the Records message to the producer actor 
// fromValuesWithKey take the kafka topic name, an optionnal key and values
producer ! ProducerRecords.fromValuesWithKey("test", Some("akey"), Seq("One", "Two"), None, None)
```

**Consumer simple implementation**

First define our Actor responsible for handling incoming records
```scala
import akka.actor.Actor
import cakesolutions.kafka.akka.ConsumerRecords
import cakesolutions.kafka.akka.KafkaConsumerActor.Confirm

class Receiver extends Actor {

  // Extractor for ensuring type safe cast of records
  val recordsExt = ConsumerRecords.extractor[String, String]

  // Akka will dispatch messages here sequentially for processing.  The next batch is prepared in parallel and can be dispatched immediately
  // after the Confirm.  Performance is only limited by processing time and network bandwidth.
  override def receive: Receive = {
    // Type safe cast of records to correct serialisation type
    case recordsExt(records) =>

      // Provide the records for processing as a sequence of tuples
      processRecords(records.pairs)

      // Or provide them using the raw Java type of ConsumerRecords[Key,Value]
      //processRecords(records.records)

      // Confirm and commit back to Kafka
      sender() ! Confirm(records.offsets)
  }

  // Process the whole batch of received records.
  // The first value in the tuple is the optional key of a record.
  // The second value in the tuple is the actual value from a record.
  def processRecords(records: Seq[(Option[String], String)]) = {
    println(">>>processRecords")

    records.map((p: (Option[String], String)) =>  println(s"Received record ${p._1} with key ${p._2} "))

  }

  // Or process the batch of records via the raw kafka client records model
  //def processRecords(records: org.apache.kafka.clients.consumer.ConsumerRecords[String, String]) = { ... }
}
```
Then we setup the ActorSystem and get a reference to a ReceiverActor, pass it to the library KafkaConsumerActor
```scala
import akka.actor.{ActorSystem, Props}
import cakesolutions.kafka.akka.KafkaConsumerActor
import cakesolutions.kafka.akka.KafkaConsumerActor.Subscribe
import com.typesafe.config.ConfigFactory
import org.apache.kafka.common.serialization.StringDeserializer

val system = ActorSystem("akkaSandboxSystem")
val conf = ConfigFactory.load()

// ActorSystem is a heavy object: create only one per application
//val system = ActorSystem("akkaSandboxSystem")

//new ReceiverActor
val receiverActor = system.actorOf(Props[Receiver], "receiverActor1")

//KafkaConsumerActor with a reference to the receiver actor
val consumer = system.actorOf(
  KafkaConsumerActor.props(conf, new StringDeserializer, new StringDeserializer, receiverActor)
)

// Send the initiating subscription to the consumer actor to the 'test' topic
consumer ! Subscribe.AutoPartition(Seq("test"))
```


### Akka-http and REST API ###
Starting with the [Lightbend tutorial](http://www.lightbend.com/activator/template/akka-http-microservice) for a quick 
start.

**Starting the microservice and see responses**

```
sbt 'runMain fr.hoshi.kafkasandbox.services.AkkaHttpMicroservice'
```

You can check out where are Google DNS servers by opening http://localhost:9000/ip/8.8.8.8. As you can see in the URL, 
the browser will send GET request to the first endpoint.

You can also check our endpoints using curl command line tool:
```bash
$ curl http://localhost:9000/ip/8.8.8.8
```
and
```bash
$ curl -X POST -H 'Content-Type: application/json' http://localhost:9000/ip -d '{"ip1": "8.8.8.8", "ip2": "8.8.4.4"}'
```
for the second endpoint.

#### Integration with kafka ####
We'll implement a new service endpoint to put request IP address on a kafka topic named "sandbox.tracking.ips". We used 
POST query with JSON data for demonstration purpose, but could've go with GET and getting client IP from the HTTP headers.
```bash
$ curl -X POST -H 'Content-Type: application/json' http://localhost:9000/tracker -d '{"ip": "8.8.8.8"}'
```
The ips topic will contains json with the following format
```json
{
  "ip": "8.8.8.8",
  "created": "2017-01-20T14:38:57+01:00"
}
```


A kafka consumer elsewhere will listen to that topic, perform the lookups for GeoLocation and publish a message to a 
second topic "sandbox.tracking.locations"

The locations topic will contains JSON with the following structure 
```json
{
  "city": "Mountain View",
  "query": "8.8.8.8",
  "country": "United States",
  "lon": -122.0838,
  "lat": 37.386
}
```

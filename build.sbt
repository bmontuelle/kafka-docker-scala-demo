name := "kafka-docker-scala-demo"

organization := "fr.hoshi.kafkasandbox"
version := "1.0"

scalaVersion := "2.11.8"


resolvers += Resolver.bintrayRepo("cakesolutions", "maven")

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")
scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

libraryDependencies ++= {
  val akkaV       = "2.4.16"
  val akkaHttpV   = "10.0.1"
  val scalaTestV  = "3.0.1"
  val scalaKafkaClientV = "0.10.1.1"
  Seq(
    "net.cakesolutions" %% "scala-kafka-client" % scalaKafkaClientV,
    "net.cakesolutions" %% "scala-kafka-client-akka" % scalaKafkaClientV,
    "com.hunorkovacs" %% "koauth" % "1.1.0",
    "org.json4s" %% "json4s-native" % "3.3.0",
    "com.typesafe.akka" %% "akka-actor" % akkaV,
    "com.typesafe.akka" %% "akka-stream" % akkaV,
    "com.typesafe.akka" %% "akka-testkit" % akkaV,
    "com.typesafe.akka" %% "akka-http" % akkaHttpV,
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpV,
    "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpV,
    "org.scalatest"     %% "scalatest" % scalaTestV % "test"
  )
}


Revolver.settings
package fr.hoshi.kafkasandbox

import cakesolutions.kafka.{KafkaConsumer, KafkaProducer}
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}

object PlainDriver {

  val consumerConf = KafkaConsumer.Conf(
    new StringDeserializer(),
    new StringDeserializer(),
    bootstrapServers = "localhost:9092",
    groupId = "group")

  // Create a org.apache.kafka.clients.producer.KafkaProducer
  def consumer = KafkaConsumer(consumerConf)

  def producer = KafkaProducer(
    KafkaProducer.Conf(
      new StringSerializer(),
      new StringSerializer(),
      bootstrapServers = "localhost:9092"
    )
  )
}
package fr.hoshi.kafkasandbox

import akka.actor.ActorSystem
import cakesolutions.kafka.akka.{KafkaProducerActor, ProducerRecords}
import com.typesafe.config.ConfigFactory
import org.apache.kafka.common.serialization.StringSerializer

object ProducerMain {

  def main(args: Array[String]): Unit = {
    val system = ActorSystem("akkaSandboxSystem")
    val conf = ConfigFactory.load()

    val producer = system.actorOf(
      KafkaProducerActor.props(conf, new StringSerializer, new StringSerializer)
    )

    // Send the initiating subscription to the consumer actor to the 'test' topic
    producer ! ProducerRecords.fromValuesWithKey("test", Some("akey"), Seq("One", "Two"), None, None)
  }

}
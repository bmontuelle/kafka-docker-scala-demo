package fr.hoshi.kafkasandbox

package sample.hello

import akka.actor.{ActorSystem, Props}
import cakesolutions.kafka.akka.KafkaConsumerActor
import cakesolutions.kafka.akka.KafkaConsumerActor.Subscribe
import com.typesafe.config.ConfigFactory
import fr.hoshi.kafkasandbox.actors.Receiver
import org.apache.kafka.common.serialization.StringDeserializer

object ReceiverMain {

  def main(args: Array[String]): Unit = {
    val system = ActorSystem("akkaSandboxSystem")
    val conf = ConfigFactory.load()

    // ActorSystem is a heavy object: create only one per application
    //val system = ActorSystem("akkaSandboxSystem")

    //new ReceiverActor
    val receiverActor = system.actorOf(Props[Receiver], "receiverActor1")

    //KafkaConsumerActor with a reference to the receiver actor
    val consumer = system.actorOf(
      KafkaConsumerActor.props(conf, new StringDeserializer, new StringDeserializer, receiverActor)
    )

    // Send the initiating subscription to the consumer actor to the 'test' topic
    consumer ! Subscribe.AutoPartition(Seq("test"))


  }



}
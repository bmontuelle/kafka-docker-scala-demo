package fr.hoshi.kafkasandbox.actors

import akka.actor.Actor
import cakesolutions.kafka.akka.ConsumerRecords
import cakesolutions.kafka.akka.KafkaConsumerActor.Confirm

class Receiver extends Actor {

  // Extractor for ensuring type safe cast of records
  val recordsExt = ConsumerRecords.extractor[String, String]

  // Akka will dispatch messages here sequentially for processing.  The next batch is prepared in parallel and can be dispatched immediately
  // after the Confirm.  Performance is only limited by processing time and network bandwidth.
  override def receive: Receive = {
    // Type safe cast of records to correct serialisation type
    case recordsExt(records) =>

      // Provide the records for processing as a sequence of tuples
      processRecords(records.pairs)

      // Or provide them using the raw Java type of ConsumerRecords[Key,Value]
      //processRecords(records.records)

      // Confirm and commit back to Kafka
      sender() ! Confirm(records.offsets)
  }

  // Process the whole batch of received records.
  // The first value in the tuple is the optional key of a record.
  // The second value in the tuple is the actual value from a record.
  def processRecords(records: Seq[(Option[String], String)]) = {
    println(">>>processRecords")

    records.map((p: (Option[String], String)) =>  println(s"Received record ${p._1} with key ${p._2} "))

  }

  // Or process the batch of records via the raw kafka client records model
  //def processRecords(records: org.apache.kafka.clients.consumer.ConsumerRecords[String, String]) = { ... }
}

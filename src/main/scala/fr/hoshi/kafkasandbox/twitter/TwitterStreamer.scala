package fr.hoshi.kafkasandbox.twitter

/**
 * This is a fully working example of Twitter's Streaming API client.
 * NOTE: this may stop working if at any point Twitter does some breaking changes to this API or the JSON structure.
 */

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpHeader.ParsingResult
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import cakesolutions.kafka.akka.{KafkaProducerActor, ProducerRecords}
import com.hunorkovacs.koauth.domain.KoauthRequest
import com.hunorkovacs.koauth.service.consumer.DefaultConsumerService
import com.typesafe.config.ConfigFactory
import org.apache.kafka.common.serialization.StringSerializer
import org.json4s._
import org.json4s.native.JsonMethods._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

object TwitterStreamer extends App {

	val conf = ConfigFactory.load()

	//Get your credentials from https://apps.twitter.com and replace the values below
	private val consumerKey = "BDGqybt8PqDaaxvOfB9QyvfbG"
	private val consumerSecret = "udpsp9rjghV9VCjBocRJrQN5vDTEjgtPkUwco8lug7Aj5TFXZy"
	private val accessToken = "862904090-zjCmT5WnyalbseXqR2hi9NNXsmTKCRTExfHPiNCr"
	private val accessTokenSecret = "yseHpBnvroPMrW7UgqSPgyftl7gHLP8OSbNQ11aEp70JP"
	private val url = "https://stream.twitter.com/1.1/statuses/filter.json"

	implicit val system = ActorSystem()
	implicit val materializer = ActorMaterializer()
	implicit val formats = DefaultFormats

	private val consumer = new DefaultConsumerService(system.dispatcher)

  private val kafkaProducer = system.actorOf(
    KafkaProducerActor.props(conf, new StringSerializer, new StringSerializer)
  )

	//Filter tweets by a term "london"
	val body = "track=Trump"
	val source = Uri(url)

	//Create Oauth 1a header
	val oauthHeader: Future[String] = consumer.createOauthenticatedRequest(
		KoauthRequest(
			method = "POST",
			url = url,
			authorizationHeader = None,
			body = Some(body)
		),
		consumerKey,
		consumerSecret,
		accessToken,
		accessTokenSecret
	) map (_.header)

	oauthHeader.onComplete {
		case Success(header) =>
			val httpHeaders: List[HttpHeader] = List(
				HttpHeader.parse("Authorization", header) match {
					case ParsingResult.Ok(h, _) => Some(h)
					case _ => None
				},
				HttpHeader.parse("Accept", "*/*") match {
					case ParsingResult.Ok(h, _) => Some(h)
					case _ => None
				}
			).flatten
			val httpRequest: HttpRequest = HttpRequest(
				method = HttpMethods.POST,
				uri = source,
				headers = httpHeaders,
				entity = HttpEntity(contentType = ContentType(MediaTypes.`application/x-www-form-urlencoded`, HttpCharsets.`UTF-8`), string = body)
			)
			val request = Http().singleRequest(httpRequest)
			request.flatMap { response =>
				if (response.status.intValue() != 200) {
					println(response.entity.dataBytes.runForeach(_.utf8String))
					Future(Unit)
				} else {
					response.entity.dataBytes
						.scan("")((acc, curr) => if (acc.contains("\r\n")) curr.utf8String else acc + curr.utf8String)
						.filter((s:String) => s.contains("\r\n"))
						.map(json => Try(parse(json).extract[Tweet]))
						.runForeach {
							case Success(tweet) =>
								println("-----")
								println(tweet.text)
                kafkaProducer ! ProducerRecords.fromValuesWithKey("test", Some(tweet.id_str), Seq(tweet.created_at, tweet.user.name, tweet.text), None, None)
							case Failure(e) =>
								//invalid json content
							  //println("-----")
								//println(e.getStackTrace)
						}
				}
			}
		case Failure(failure) => println(failure.getMessage)
	}

}
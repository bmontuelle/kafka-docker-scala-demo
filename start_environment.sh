#!/usr/bin/env bash

#Create docker mapped volumes
mkdir -p ./data/kafka/
mkdir -p ./data/mongo/
mkdir -p ./data/logs/

#start services containers
docker-compose up

#Retrieve assigned IP adresses from docker
export ZK_IP=$(docker inspect --format '{{ .NetworkSettings.Networks.kafkadockerscalademo_default.IPAddress }}' kafkadockerscalademo_zookeeper_1)
export KAFKA_IP=$(docker inspect --format '{{ .NetworkSettings.Networks.kafkadockerscalademo_default.IPAddress }}' kafkadockerscalademo_kafka_1)
export MONGO_IP=$(docker inspect --format '{{ .NetworkSettings.Networks.kafkadockerscalademo_default.IPAddress }}' kafkadockerscalademo_mongo_1)

#Run another kafka container to create the topics
docker run --rm --net kafkadockerscalademo_default -e "ZOOKEEPER_IP=$ZK_IP" ches/kafka \
    kafka-topics.sh --create --topic test --replication-factor 1 --partitions 1 --zookeeper $ZK_IP:2181 && \
    kafka-topics.sh --create --topic ips --replication-factor 1 --partitions 1 --zookeeper $ZK_IP:2181

